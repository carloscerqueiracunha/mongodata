package br.com.application.mongodata;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongodataApplication implements CommandLineRunner {

	@Autowired
	private ItemRepository repository;
	
	public static void main(String[] args) {
		SpringApplication.run(MongodataApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		repository.deleteAll();
		
		// Save a list of items.
		repository.saveAll(Arrays.asList(new Item("Personal Computer", 2685.50, 100), 
				new Item("Diatonic Harmonica", 200.0, 45),
				new Item("Fan", 135.49, 2)));
		
		// Print the list.
		System.out.println("List with all Items:");
		for (Item item : repository.findAll()) {
			System.out.println(item);
		}
		
		// Fetch an individual item.
		System.out.println("Item found by searching name \"Fan\"");
		System.out.println(repository.findByName("Fan"));
		
	}

}
