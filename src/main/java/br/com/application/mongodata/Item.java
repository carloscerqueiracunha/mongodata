package br.com.application.mongodata;

import org.springframework.data.annotation.Id;

public class Item {

	@Id
	public String id;
	
	public String name;
	public Double price;
	public Integer quantity;
	
	public Item() {
	}
	
	public Item(String name, Double price, Integer quantity) {
		this.name = name;
		this.price = price;
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return String.format("Item [id=%s, name=%s, price=%s, quantity=%s]", id, name, price, quantity);
	}

}
